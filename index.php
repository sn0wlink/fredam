<?php
    
    /*
        -= FreDam =-
        
        Written by: David Collins-Cubitt
        URL: http://www.sn0wlink.com
        Based on: FredMin Framework
        Web: https://github.com/sn0wlink/FreDam
    */

// Include Files
        include ('config.php');
        include ('functions.php');
        include ('layout.php');
?>