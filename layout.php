<html>
<head>
    <link rel='icon' href='favicon.ico' type='image/x-icon' />
    <link rel='stylesheet' href='style.css' type='text/css'>
    <title>FreDAM - <?= $SiteName ?></title>
</head>

<body>
    <div class='header'>
        <img class='logo' src='images/header.png'>
        <center><h3><?= $SiteName ?></h3></center>
    </div>

    <div class='content'>

    <?php 
        // Scan and build module list
        BuildModules(); 

        // Build page footer
        PageFooter();
    ?>

</body>

</html>