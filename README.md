# FreDAM

A FOSS digital asset management program for the organisation and control of 
stock photography and their licences.

# Demo Screenshots
![Demo Screenshot](Docs/screenshot.gif)

## Installation
You will need a working LAMPP stack. Then just run the setup file.

## Contribution Rules
- 4 Spaces - NOT tabs... (please)
- 80 char width soft limit (it just looks nice)
- Be excellent to eachother! (it's the LAW, so deal with it.)

## Credits
Sample images included are provided by Unsplash. Go to [Unslash.com](https://unsplash.com/) for royalty
free content.