<?php

// Display Debug Information
$DisplayDebug = TRUE; // FALSE for production

// Database Creds
$DBserver = "localhost";
$DBusername = "FreDAMuser";
$DBpassword = "FreDAMpassword";
$DBname = "FreDAM";

// Setup Site Name
$SiteName = "Digital Asset Management";

// Location where images are stored
$StorageLocation = "Sample-Data";

?>