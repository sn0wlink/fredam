<?php

function PageHeader() {
    global $AppName;

echo"
    <html>
<head>
    <link rel='icon' href='favicon.ico' type='image/x-icon' />
    <link rel='stylesheet' href='../../../style.css' type='text/css'>
    <title>FreDAM - $AppName</title>
</head>

<body>
    <div class='header'>
        <a href='../../../index.php'>
        <img class='logo' src='../../../images/header.png'>
        </a>
        <center><h3>$AppName</h3></center>
        <a class='back' href='../../../index.php'>Back</a>
    </div>

    <div class='content'>
    ";
}

function PageFooter() {
    echo "
    </div>

    <div class='page-footer'>
        FreDAM - Written by David Collins-Cubitt - Open Source Digital Asset Management Software
    </div>
</html>
    ";
}


// List Custom Module Names (alphabetical order)
function BuildModules() {

    $Path = "Modules";
    $directories = glob($Path . '/*' , GLOB_ONLYDIR);
    
    foreach($directories as $key => $val){
        $header = str_ireplace("Modules/","",$val);
        echo "<p class='menu-header'>$header</p>";
    
    
        $subdirectories = glob($val . '/*' , GLOB_ONLYDIR);
        foreach($subdirectories as $key => $val2){
            include ("$val2/app-config.php");

            $link = str_ireplace("$val/","",$val2);

            echo "
            <div class='icon'>
            <a class='icon' href='$val2'>
            <img class='icon' src='$val2/$Icon'><br />
            $AppName
            </a>
            </div>
            ";
        }
    }

}

// Compression Function 
// (Acording to Imagick Docs, with some extra calculations)
function ImageCompress ($FileInput, $FileOutput, $FinalHeight, $FinalWidth) {
    
    // Get current dimenstions
    $CurrentDimensions = getimagesize($FileInput);
    $Width = $CurrentDimensions[0];
    $Height = $CurrentDimensions[1];

    // Calculate reduction ratio
    $Ratio = $Width/$Height;
    
    // Calculate final image width
    $FinalWidth = $FinalHeight * $Ratio;

    // Fix float point errors
    $FinalWidth = round($FinalWidth);

    // The compression part:
    $thumb = new Imagick();
    $thumb->readImage($FileInput);    
    $thumb->resizeImage($FinalWidth,$FinalHeight,Imagick::FILTER_LANCZOS,1);
    $thumb->writeImage("$FileOutput");
    $thumb->clear();
    $thumb->destroy();
}

?>