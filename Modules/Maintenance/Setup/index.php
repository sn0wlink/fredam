 <?php

 /* 
    FreDAM Setup Module
    ----------------------------
    Created by: Teal Press Ltd
    Date: 27/03/2020
    Author: David Collins-Cubitt
 */

// Pull main config files
include ('../../../config.php');
include ('../../../functions.php');

// Include Application Config
include ('app-config.php');

// Build page layout
PageHeader();

// Layout page UI
echo "
<form action='index.php' method='post'>
    <input type='submit' name='dbCreate' value='Setup Database' />
</form>
";

if (isset($_POST['dbCreate'])) {

    // Create connection
    $dbConnect = new mysqli($DBserver, $DBusername, $DBpassword);

    // Create database
    $dbCreate = "CREATE DATABASE $DBname";
    mysqli_query($dbConnect, $dbCreate);

    // Create connection for table
    $dbConnect = new mysqli($DBserver, $DBusername, $DBpassword, $DBname);

    // sql to create table
    $MkTable = "CREATE TABLE fd_photos (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    orig_filename VARCHAR(1000),
    filename VARCHAR(1000),  
    location VARCHAR(1000),
    filetype VARCHAR(15),
    filesize VARCHAR(20),
    version VARCHAR(10),
    artist VARCHAR(100),
    purchased_from VARCHAR(100),
    incorporated_projects VARCHAR(1000),
    notes VARCHAR(1000),
    tags VARCHAR(1000),
    licence_end VARCHAR(8),
    date_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )";

    mysqli_query($dbConnect, $MkTable);

    mysqli_close($dbConnect);

    // Print success
    echo "<h3>Setup Complete</h3>";
}

// Build page footer
PageFooter();

?>