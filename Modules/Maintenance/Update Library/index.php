 <?php

 /* 
    Preview Module
    ----------------------------
    Author: David Collins-Cubitt
 */

// Pull main config files
include ('../../../config.php');
include ('../../../functions.php');

// Include Application Config
include ('app-config.php');

// Build page layout
PageHeader();

// Place your application code here!
// V V V V V V V

foreach (glob("../../../$StorageLocation/*") as $ImageSource) {


    // Set final maximum image height
    $FinalHeight = 60;

    $Filename = str_replace("../../../$StorageLocation/", "", $ImageSource);

    $RealLocation = realpath("../../../$StorageLocation");
    $FileInput = "$RealLocation/$Filename";
    $FileOutput = "../../../images/thumbnails/$Filename";

    // Check file exists
    if (file_exists($FileOutput) != TRUE) {
        echo "Image Compressed: $Filename <br />";

        // Make sure its a jpg file
        if(strpos($Filename, ".jpg") !== false) {
            ImageCompress ($FileInput, $FileOutput, $FinalHeight, $FinalWidth);
        }
    }
}

echo "<p><h3>Processing Complete!</h3></p>";

// Build page footer
PageFooter();

?> 

