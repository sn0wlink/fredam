 <?php

 /* 
    FreDAM Core Module
    ----------------------------
    Created by: Teal Press Ltd
    Date: 27/03/2020
    Author: David Collins-Cubitt
 */

// Pull main config files
include ('../../../config.php');
include ('../../../functions.php');

// Include Application Config
include ('app-config.php');

// Build page layout
PageHeader();
?>

<!-- Webpage Layout -->
<center><h3>Photo Upload</h3></center>

<form action='index.php' method='post' enctype="multipart/form-data">
    <!-- File Selection -->
    <p>
     <input type="file" name="FileToUpload" id="FileToUpload"><br />
    </p>

    <input class='data-input' type='text' name='version' placeholder='File Version'> CMYK, SRGB etc<br />
    <input class='data-input' type='text' name='artist' placeholder='Artist Name'> Artist Name<br />
    <input class='data-input' type='text' name='purchased_from' placeholder='purchased_from'> Supplier of artwork<br />
    <input class='data-input' type='text' name='incorporated_projects' placeholder='incorporated_projects'> Projects used in<br />
    <input class='data-input' type='date' name='licence_end'> Licence Expiry Date (Keep empty for Lifetime)<br />

    <br />Licencing Notes:<br />
    <textarea class='data-input' type='textbox' name='notes' placeholder='Notes'></textarea> <br />

    <br />Photo Tags:<br />
    <textarea class='data-input' type='textbox' name='tags' placeholder='Tags'></textarea> <br />

    <p>
    <input class='data-input' type='submit' name='FDPhotoUpload' value='Upload' />
    </p>
</form>

<?php

// If button pressed...
if (isset($_POST['FDPhotoUpload'])) {


    // Convert variables to something useful
    // *Smashes face against keyboard*
    $SelectedFile = $_POST['FileToUpload'];
    $version = $_POST['version'];
    $artist = $_POST['artist'];
    $purchased_from = $_POST['purchased_from'];
    $incorporated_projects = $_POST['incorporated_projects'];
    $notes = $_POST['notes'];
    $licence_end = $_POST['licence_end'];
    $orig_filename =  $_FILES['FileToUpload']['name'];
    $filename = $_FILES['FileToUpload']['name'];
    $tempfile = $_FILES['FileToUpload']['tmp_name'];
    $location = realpath("../../../$StorageLocation/");
    $filetype = $_FILES['FileToUpload']['type'];
    $filesize = $_FILES['FileToUpload']['size'];
    $tags = $_POST['tags'];

    // Where will the file be uploaded

    $UploadDir = realpath("../../../$StorageLocation/");
    $uploadfile = "$UploadDir/$filename";
    

    // Detect if licence field is empty
    if ($licence_end == "") {
        $licence_end = "LIFETIME";
    }

    // Create connection
    $dbConnect = mysqli_connect($DBserver, $DBusername, $DBpassword, $DBname);
    
    // Setup the data to pipe to mysql
    $sqlData = "INSERT INTO fd_photos (
        orig_filename, 
        filename, 
        location, 
        filetype, 
        filesize, 
        version, 
        artist,
        purchased_from, 
        incorporated_projects, 
        notes, 
        tags,
        licence_end) 
        VALUES (
        '$orig_filename',
        '$filename',
        '$location',
        '$filetype',
        '$filesize',
        '$version',
        '$artist',
        '$purchased_from',
        '$incorporated_projects',
        '$notes',
        '$tags',
        '$licence_end'
    )";

    // SQL Data to Databas
    mysqli_query($dbConnect, $sqlData);
    mysqli_close($dbConnect);

    // Confirm upload
    echo "<p><strong>Data uploaded.</strong></p>";

    ///////////////////////////////////////////////
    // UPLOAD IMAGE 
    ///////////////////////////////////////////////

    // Place buffered file into correct folder
    move_uploaded_file($tempfile, $uploadfile);

    ///////////////////////////////////////////////

    // Setup Debug Information
    if ($DisplayDebug) {
        echo "<pre>";
        print_r($_FILES);
        echo "</pre>";

        echo "orig_filename: $orig_filename <br />";
        echo "filename: $filename <br />";
        echo "location: $location <br />";
        echo "filetype: $filetype <br />";
        echo "filesize: $filesize <br />";
        echo "version: $version <br />";
        echo "purchased_from: $purchased_from <br />";
        echo "incorporated_projects: $incorporated_projects <br />";
        echo "notes: $notes <br />";
        echo "licence_end: $licence_end <br />";
    }
}

// Build page footer
PageFooter();

?> 