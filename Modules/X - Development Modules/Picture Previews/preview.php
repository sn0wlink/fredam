 <?php

 /* 
    Preview Module
    ----------------------------
    Author: David Collins-Cubitt
 */

// Pull main config files
include ('../../../config.php');
include ('../../../functions.php');

// Include Application Config
include ('app-config.php');

// Build page layout
PageHeader();

// Setup Variables
$Location = "../../../$StorageLocation/";
$filename = $_GET['file'];

// Setup Database Connection and search strings
$dbconnect = mysqli_connect($DBserver, $DBusername, $DBpassword, $DBname);
$SearchStr = "SELECT * FROM fd_photos WHERE (filename LIKE '$filename%')";
$DBQuery = mysqli_query($dbconnect, $SearchStr);

// Pull required data
while($row = mysqli_fetch_assoc($DBQuery)) {
    $filename = $row['filename'];
    $filetype = $row['filetype'];
    $filesize = $row['filesize'];
    $artist = $row['artist'];
    $purchased_from = $row['purchased_from'];
    $incorporated_projects = $row['incorporated_projects'];
    $notes = $row['notes'];
    $tags = $row['tags'];
    $licence_end = $row['licence_end'];
}

// clean up fileype displayed
$filetype = str_replace('image/', '', $filetype);

// Clean File Size
$filesize = round($filesize/1048576, 2);

?>

<style>

    div.picture-preview {
        height: 400px;
    }

    img.preview {
        display: inline-block;
        max-width: 45%;
        padding-right: 40px;
        padding-bottom: 40px;
    }

    div.info {
        display: inline-block;
        width: 45%;
        vertical-align: top;
    }

</style>

<a href='index.php'>Back to photos</a><br /><br />

<div class='picture-preview'>
    <a href='<?php echo "$Location$filename"; ?>'>
    <img class ='preview' src='<?php echo "$Location$filename"; ?>' />
    </a>

    <div class='info'>
        Artist Name: <?=$artist?><br />
        Filename: <?=$filename?><br />
        File Type: <?=$filetype?><br />
        File Size: <?=$filesize?>MB<br />
        Purchased From: <?=$purchased_from?><br />
        Incorporated Projects: <?=$incorporated_projects?><br />
        Licence Expires: <?=$licence_end?><br />
        Tags: <?=$tags?><br />
        Notes:<br /><pre><?=$notes?></pre><br />
        <br />
        <a href='edit.php?file=<?=$filename?>'>Edit</a>
    </div>    
</div>

<?php
// Build page footer
PageFooter();

?> 
 
