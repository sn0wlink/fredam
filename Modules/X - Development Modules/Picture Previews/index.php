 <?php

 /* 
    Preview Module
    ----------------------------
    Author: David Collins-Cubitt
 */

// Pull main config files
include ('../../../config.php');
include ('../../../functions.php');

// Include Application Config
include ('app-config.php');

// Build page layout
PageHeader();

foreach (glob("../../../images/thumbnails/*") as $ImageLocation) {
    
    // Image filename
    $ImageName = str_replace("../../../images/thumbnails/", "", $ImageLocation);

    // Work out http filepath
	$ImageRef = "../../../images/thumbnails/$ImageName";

    echo "
    <div class='icon'>
    <a class='icon' href='preview.php?file=$ImageName'>
    <img src='$ImageRef'><br />
    </a>
    </div>
    ";
}

// Build page footer
PageFooter();

?> 

