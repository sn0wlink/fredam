 <?php

 /* 
    Preview Module
    ----------------------------
    Author: David Collins-Cubitt
 */

// Pull main config files
include ('../../../config.php');
include ('../../../functions.php');

// Include Application Config
include ('app-config.php');

// Build page layout
PageHeader();

// Setup Variables
$Location = "../../../$StorageLocation/";
$filename = $_GET['file'];

// Setup Database Connection and search strings
$dbconnect = mysqli_connect($DBserver, $DBusername, $DBpassword, $DBname);
$SearchStr = "SELECT * FROM fd_photos WHERE (filename LIKE '$filename%')";
$DBQuery = mysqli_query($dbconnect, $SearchStr);
// Shhh... You'll wake Fluffy!

// Pull required data
while($row = mysqli_fetch_assoc($DBQuery)) {
    $filename = $row['filename'];
    $filetype = $row['filetype'];
    $filesize = $row['filesize'];
    $artist = $row['artist'];
    $purchased_from = $row['purchased_from'];
    $incorporated_projects = $row['incorporated_projects'];
    $notes = $row['notes'];
    $tags = $row['tags'];
    $licence_end = $row['licence_end'];
}

// Clean up fileype displayed
$filetype = str_replace('image/', '', $filetype);

// Clean File Size
$filesize = round($filesize/1048576, 2);

?>

<!-- Give me some style -->
<style>

    div.picture-preview {
        height: 400px;
    }

    img.preview {
        display: inline-block;
        max-width: 45%;
        padding-right: 40px;
        padding-bottom: 40px;
    }

    div.info {
        display: inline-block;
        width: 45%;
        vertical-align: top;
    }

</style>

<a href='index.php'>Back to photos</a><br /><br />

<div class='picture-preview'>
    <img class ='preview' src='<?php echo "$Location$filename"; ?>' />

    <div class='info'>
        <form action="save-data.php/?file=<?=$filename?>" method='post' enctype="multipart/form-data">

        Artist Name: 
        <input class='data-input' type='text' name='artist' value='<?=$artist?>'><br />

        Purchased From: 
        <input class='data-input' type='text' name='artist' value='<?=$purchased_from?>'><br />

        Incorporated Projects:
        <input class='data-input' type='text' name='artist' value='<?=$incorporated_projects?>'><br />

        Licence Expires:
        <input class='data-input' type='text' name='artist' value='<?=$licence_end?>'><br />

        Tags: <br />
        <textarea class='data-input' type='textbox' name='tags' placeholder='Tags'><?=$tags?></textarea><br />

        Notes: <br />
        <textarea class='data-input' type='textbox' name='notes' placeholder='Notes'><?=$notes?></textarea><br />

        <input class='data-input' type='submit' name='PhotoUpdate' value='Save' />
        </form>
    </div>    
</div>

<!-- TEST DATA UPLOAD AREA -->



<!-- END TEST DATA UPLOAD AREA -->

<?php
// Build page footer
PageFooter();

?> 
 
