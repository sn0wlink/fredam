 <?php

 /* 
    Preview Module
    ----------------------------
    Author: David Collins-Cubitt
 */

echo "<meta http-equiv='refresh' content='10; url=index.php'>";

// Pull main config files
include ('../../../config.php');
include ('../../../functions.php');

// Include Application Config
include ('app-config.php');

// Build page layout
PageHeader();

// Start page content here
echo "Saving Data...";

// Build page footer
PageFooter();

?> 

